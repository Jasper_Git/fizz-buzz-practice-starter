package tdd.fizzbuzz;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class FizzBuzzTest {

    @Test
    public void should_return_normal_number_string_when_given_a_normal_number() {
        int n = 1;
        FizzBuzz fizzBuzz = new FizzBuzz();
        String str = fizzBuzz.countOff(1);

        Assertions.assertEquals(String.valueOf(n), str);
    }

    @Test
    public void should_return_Fizz_when_given_a_multiple_of_3() {
        int n = 3;
        FizzBuzz fizzBuzz = new FizzBuzz();
        String str = fizzBuzz.countOff(n);

        Assertions.assertEquals("Fizz", str);
    }

    @Test
    public void should_return_Buzz_when_given_a_multiple_of_5() {
        int n = 5;
        FizzBuzz fizzBuzz = new FizzBuzz();
        String str = fizzBuzz.countOff(n);

        Assertions.assertEquals("Buzz", str);
    }

    @Test
    public void should_return_WHIZZ_when_given_a_multiple_of_7(){
        int n = 7;
        FizzBuzz fizzBuzz = new FizzBuzz();
        String str = fizzBuzz.countOff(n);

        Assertions.assertEquals("Whizz", str);
    }

    @Test
    public void should_return_FizzBuzz_when_given_a_multiple_of_3_and_5() {
        int n = 15;
        FizzBuzz fizzBuzz = new FizzBuzz();
        String str = fizzBuzz.countOff(n);

        Assertions.assertEquals("FizzBuzz", str);
    }

    @Test
    public void should_return_FizzWhizz_when_given_a_multiple_of_3_and_7() {
        int n = 21;
        FizzBuzz fizzBuzz = new FizzBuzz();
        String str = fizzBuzz.countOff(n);

        Assertions.assertEquals("FizzWhizz", str);
    }

    @Test
    public void should_return_BuzzWhizz_when_given_a_multiple_of_5_and_7() {
        int n = 35;
        FizzBuzz fizzBuzz = new FizzBuzz();
        String str = fizzBuzz.countOff(n);

        Assertions.assertEquals("BuzzWhizz", str);
    }

    @Test
    public void should_return_FizzBuzzWhizz_when_given_a_multiple_of_3_and_5_and_7() {
        int n = 105;
        FizzBuzz fizzBuzz = new FizzBuzz();
        String str = fizzBuzz.countOff(n);

        Assertions.assertEquals("FizzBuzzWhizz", str);
    }
}
