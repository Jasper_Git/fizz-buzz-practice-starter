package tdd.fizzbuzz;

public class FizzBuzz {


    private static final String FIZZ = "Fizz";
    private static final String BUZZ = "Buzz";

    private static final String WHIZZ = "Whizz";

    public String countOff(int i) {
        if (i % 105 == 0) {
            return this.FIZZ + this.BUZZ + this.WHIZZ;
        }

        if (i % 35 == 0) {
            return this.BUZZ + this.WHIZZ;
        }

        if (i % 21 == 0) {
            return this.FIZZ + this.WHIZZ;
        }

        if (i % 15 == 0) {
            return this.FIZZ + this.BUZZ;
        }
        if (i % 3 == 0) {
            return FIZZ;
        }
        if (i % 5 == 0) {
            return BUZZ;
        }

        if (i % 7 == 0) {
            return this.WHIZZ;
        }
        return String.valueOf(i);
    }
}
